import csv
import unittest
from csv import DictReader
from datetime import datetime, date

from dateutil.parser import parse, isoparse
from app.utils import detect_types, get_value_in_columns, Processing

PATH_TO_TEST_CSV = 'TEST_DATA.csv'


class TestHelpingFunctions(unittest.TestCase):
    """Тестирование вспомогательных функций"""

    def setUp(self) -> None:
        with open(PATH_TO_TEST_CSV) as f_n:
            f_n_reader: DictReader = csv.DictReader(f_n)
            self.test_data = list(f_n_reader)

    def test_detect_types(self) -> None:
        for item in self.test_data:
            self.assertIs(type(detect_types(item['int'])), int)
            self.assertIs(type(detect_types(item['float'])), float)
            self.assertIs(type(detect_types(item['string'])), str)
            self.assertIs(type(detect_types(item['datetime'])), datetime)
            self.assertIs(type(detect_types(item['date'])), date)


class TestProcessing(unittest.TestCase):
    """Тестирование класса обработки данных"""

    def setUp(self) -> None:
        self.testClass = Processing(PATH_TO_TEST_CSV)

    def test_get_items(self):
        all_items = self.testClass.get_items()
        column = self.testClass.get_items()
