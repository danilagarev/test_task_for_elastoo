import uvicorn
from fastapi import FastAPI, HTTPException

from app.utils import Processing

app = FastAPI()
PATH_TO_CSV = 'MOCK_DATA.csv'


@app.get('/items/')
async def get_item(column: str = None, q: str = None):
    data = Processing(path_file=PATH_TO_CSV)
    if column:
        items = data.processing_column(column, q)
        if items[0]["Error"]:
            raise HTTPException(status_code=400, detail=items[0]["Error"])

        return items
    else:
        items = data.get_items()
        return items


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
